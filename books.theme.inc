<?php

/*
* hook_menu to load tpl files from theme folder
 * i.e sites/all/themes/custom/{current_theme_name}/templates/books
 * current_theme_name = bartik 
 */
function books_theme() {

  return array(
		'books_list' => array(
		  'template' => 'books-list',
		  'path' => drupal_get_path('module', 'books') . '/theme'
		),
	  );
	
}