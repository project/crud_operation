 <?php
  /**
  * Implement hook_views_data
  *
  */

   function books_views_data() {
  $data = array();
    $data['books']['table']['group'] = t('Books');

     $data['books']['table']['base'] = array(
      'field' => 'bid', // This is the identifier field for the view.
      'title' => t('Books record'),
      'help' => t('This table is for final'),
      'weight' => -10,
      'description' => 'This is books views .',
    );

      $data['books']['books_title'] = array(
          'title' => t('Books title'),
          'help' => t('book title'),
          'filter' => array(
              'handler' => 'views_handler_filter_string',
          ),
          'field' => array(
              'handler' => 'views_handler_field',
              'click sortable' => TRUE,
          ),
          'sort' => array(
              'handler' => 'views_handler_sort',
          ),
          'argument' => array(
              'handler' => 'views_handler_argument',
          ),
      ); 
     $data['books']['books_author'] = array(
          'title' => t('Book Author'),
          'help' => t('Add Book Author for view'),
          'field' => array(
              'handler' => 'views_handler_field',
              'click sortable' => TRUE,
          ),
          'filter' => array(
              'handler' => 'views_handler_filter_string',
          ),
          'sort' => array(
              'handler' => 'views_handler_sort',
          ),
          'argument' => array(
              'handler' => 'views_handler_argument',
          ),
          );
     $data['books']['books_description'] = array(
          'title' => t('Book Description'),
          'help' => t('Add book description'),
          'field' => array(
              'handler' => 'views_handler_field',
              'click sortable' => TRUE,
          ),
          'filter' => array(
              'handler' => 'views_handler_filter_string',
          ),
          'sort' => array(
              'handler' => 'views_handler_sort',
          ),
          'argument' => array(
              'handler' => 'views_handler_argument',
          ),
          );
	$data['books']['is_published'] = array(
          'title' => t('Book Published'),
          'help' => t('Add book published'),
          'field' => array(
              'handler' => 'views_handler_field',
              'click sortable' => TRUE,
          ),
          'filter' => array(
              'handler' => 'views_handler_filter_string',
          ),
          'sort' => array(
              'handler' => 'views_handler_sort',
          ),
          'argument' => array(
              'handler' => 'views_handler_argument',
          ),
          );

     return $data;
  }
